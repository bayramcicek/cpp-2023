#include <functional>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <string>
#include <thread>
#include <time.h>
#include <vector>

#define MULTI 11
#define SIZE 2000000

void fill(std::vector<unsigned long long int> &v, std::size_t n)
{
    int sum{ 0 };
    for (std::size_t i = 0; i < n; ++i)
    {
        for (std::size_t j = 0; j < 100; ++j)
        {
            sum += sqrt(i * j);
        }
    }
    v.push_back(sum);
}

void showPerformance(clock_t t, std::string s)
{
    std::cout << s << " res: " << std::setprecision(12)
              << static_cast<float>(t) / CLOCKS_PER_SEC << '\n';
}

int main()
{
    std::vector<unsigned long long int> v1, v2, v3, v4;
    v1.reserve(SIZE);
    v2.reserve(SIZE);
    v3.reserve(SIZE);
    v4.reserve(SIZE);

#if MULTI == 1
    // normal runnig mode
    clock_t t{ clock() };
    fill(v1, SIZE);
    fill(v2, SIZE);
    fill(v3, SIZE);
    fill(v4, SIZE);

    t = clock() - t;

    showPerformance(t, "Normal");

#else
    // thread running mode
    clock_t t{ clock() };
    std::thread first{ fill, std::ref(v1), SIZE };
    std::thread second{ fill, std::ref(v2), SIZE };
    std::thread third{ fill, std::ref(v3), SIZE };
    std::thread fourth{ fill, std::ref(v4), SIZE };

    first.join();
    second.join();
    third.join();
    fourth.join();

    t = clock() - t;

    showPerformance(t, "Thread: ");

#endif

    return 0;
}
