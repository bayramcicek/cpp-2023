#include <iostream>
#include <thread>

int main()
{
    []() { std::cout << "selamlar" << '\n'; }();

    class lambda1
    {
    public:
        void operator()() { std::cout << "lambda1 is calling " << '\n'; }
    };

    lambda1()();
    lambda1 l;
    l.operator()();

    auto a{ [](int val)
            {
                return val;
            } };

    std::cout << a(89) << '\n';

    return 0;
}