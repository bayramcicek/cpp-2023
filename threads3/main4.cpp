#include <iostream>
#include <mutex>
#include <thread>

std::mutex m1;
using namespace std::literals::chrono_literals;

void forFunc()
{
    for (int i = 0; i < 5; ++i)
    {
        m1.lock();
        std::cout << "Func i : " << i << '\n';
        m1.unlock();
        std::this_thread::sleep_for(1000ms);
    }
}

int main()
{
    std::thread worker{ &forFunc };

    for (int i = -20; i < -15; ++i)
    {
        m1.lock();
        std::cout << "main: " << i << '\n';
        m1.unlock();
        std::this_thread::sleep_for(1000ms);
    }

    worker.join();
    return 0;
}