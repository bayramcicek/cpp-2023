#ifndef __THREAD_LIB_H
#define __THREAD_LIB_H

#include <functional>
#include <iomanip>
#include <iostream>
#include <math.h>
#include <string>
#include <thread>
#include <time.h>
#include <vector>

void showPerformance(clock_t t, std::string s);
int **initializeMatrices(int row, int column);
void fill(int **arr, int row, int column, int number);
void display(int **arr, int row, int column);

void deleteMemory(int **arr, int row, int column);

#endif