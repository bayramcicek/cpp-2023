#include "threadLibrary.h"

void showPerformance(clock_t t, std::string s)
{
    std::cout << s << " res: " << std::setprecision(12)
              << static_cast<float>(t) / CLOCKS_PER_SEC << '\n';
}
int **initializeMatrices(int row, int column)
{
    int **arr{ new int *[row] };

    for (int i{ 0 }; i < row; ++i)
    {
        arr[i] = new int[column];
    }

    return arr;
}
void fill(int **arr, int row, int column, int number)
{
    for (int i{ 0 }; i < row; ++i)
    {
        for (int k{ 0 }; k < column; ++k)
        {
            int sum{ 0 };
            for (int j{ 0 }; j < number; ++j)
            {
                sum += i * k;
            }
            arr[i][k] = sum;
        }
    }
}
void display(int **arr, int row, int column)
{
    for (int i{ 0 }; i < row; ++i)
    {
        for (int j{ 0 }; j < column; ++j)
        {
            std::cout << arr[i][j] << ' ';
        }
        std::cout << '\n';
    }
}

void deleteMemory(int **arr, int row, int column)
{
    for (int i{ 0 }; i < row; ++i)
    {
        delete arr[i];
    }
    delete arr;
}
