#include <chrono>
#include <iostream>
#include <thread>

using namespace std::chrono_literals;

void myThread1()
{
    for (int i{ 0 }; i < 10; ++i)
    {
        std::cout << i << ' ' << '\n';
        std::this_thread::sleep_for(100ms);
    }
}

void myThread2()
{
    for (int i{ 20 }; i < 30; ++i)
    {
        std::cout << i << ' ' << '\n';
        std::this_thread::sleep_for(100ms);
    }
}

int main()
{
    std::thread thread1(&myThread1);
    std::thread thread2(&myThread2);
    thread1.join();
    thread2.join();

    std::cout << "selam" << '\n';
    return 0;
}