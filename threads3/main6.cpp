#include "threadLibrary.h"

#define MULTI 11
#define ROW 1920
#define COLUMN 1080
#define PROCESS_NUMBER 100

int main()
{
    int **ArrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **BrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **CrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **DrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **ErrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **FrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **GrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **HrrayMatrices{ initializeMatrices(ROW, COLUMN) };
    int **IrrayMatrices{ initializeMatrices(ROW, COLUMN) };

#if MULTI == 1

    clock_t t = clock();

    fill(ArrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(BrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(CrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(DrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(ErrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(FrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(GrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(HrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);
    fill(IrrayMatrices, ROW, COLUMN, PROCESS_NUMBER);

    t = clock() - t;
    showPerformance(t, "normal: ");

    deleteMemory(ArrayMatrices, ROW, COLUMN);
    deleteMemory(BrrayMatrices, ROW, COLUMN);
    deleteMemory(CrrayMatrices, ROW, COLUMN);
    deleteMemory(DrrayMatrices, ROW, COLUMN);
    deleteMemory(ErrayMatrices, ROW, COLUMN);
    deleteMemory(FrrayMatrices, ROW, COLUMN);
    deleteMemory(GrrayMatrices, ROW, COLUMN);
    deleteMemory(HrrayMatrices, ROW, COLUMN);
    deleteMemory(IrrayMatrices, ROW, COLUMN);

#else

    clock_t t = clock();
    std::thread first{ fill, ArrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread second{ fill, BrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread third{ fill, CrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread fourth{ fill, DrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread fifth{ fill, ErrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread sixth{ fill, FrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread seventh{ fill, GrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread eighth{ fill, HrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };
    std::thread ninth{ fill, IrrayMatrices, ROW, COLUMN, PROCESS_NUMBER };

    first.join();
    second.join();
    third.join();
    fourth.join();
    fifth.join();
    sixth.join();
    seventh.join();
    eighth.join();
    ninth.join();

    t = clock() - t;

    showPerformance(t, "Thread operation");

    deleteMemory(ArrayMatrices, ROW, COLUMN);
    deleteMemory(BrrayMatrices, ROW, COLUMN);
    deleteMemory(CrrayMatrices, ROW, COLUMN);
    deleteMemory(DrrayMatrices, ROW, COLUMN);
    deleteMemory(ErrayMatrices, ROW, COLUMN);
    deleteMemory(FrrayMatrices, ROW, COLUMN);
    deleteMemory(GrrayMatrices, ROW, COLUMN);
    deleteMemory(HrrayMatrices, ROW, COLUMN);
    deleteMemory(IrrayMatrices, ROW, COLUMN);

#endif

    return 0;
}