#include <iostream>
#include <iterator>

void calcLength(int array[])
{
    //std::cout << sizeof(array) / sizeof(array[0]) << '\n';
}

int main()
{
    int array[]{ 1, 2, 3, 4, 5, 6, 7 };
    std::cout << sizeof(array) / sizeof(array[0]) << '\n';
    calcLength(array);

    for (int index{ 0 }; index < (static_cast<int>(std::size(array))); ++index)
    {
        std::cout << array[index] << ' ';
    }

    return 0;
}