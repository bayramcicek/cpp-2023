#include <algorithm>
#include <iostream>
#include <iterator>
#include <string>

int main()
{
    std::cout << "How many names would you like to enter?: ";
    int num{};
    std::cin >> num;

    auto *array{ new std::string[num]{} };

    for (int i{ 0 }; i < num; ++i)
    {
        std::cout << "Enter name #" << i + 1 << ": ";
        std::string str{};
        std::getline(std::cin >> std::ws, str);

        array[i] = str;
    }

    std::sort(array, array + num);

    for (int i{ 0 }; i < num; ++i)
    {
        std::cout << *(array + i) << '\n';
    }

    delete[] array;

    return 0;
}