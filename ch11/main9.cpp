#include <iostream>

int main()
{
    int array[3]{ 42, 53, 97 };

    std::cout << array << '\n';
    std::cout << &array << '\n'; // int(*)[5]
    std::cout << *array << '\n';
    std::cout << array[0] << '\n';
    std::cout << &(array[0]) << '\n';
    std::cout << &(*array) << '\n';

    char ch[]{ "Jason" };
    std::cout << *ch << '\n';

    int *ptr{ array };
    std::cout << ptr[1] << '\n';

    return 0;
}