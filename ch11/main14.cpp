#include <iostream>

int main()
{
    int *ptr1{ new int{ 5 } };
    std::cout << *ptr1 << '\n';
    int *ptr2 {ptr1};

    delete ptr1;
    delete ptr2;
    std::cout << *ptr1 << '\n';
    std::cout << *ptr2 << '\n';
    // delete ptr1;
    int *array{ new int[10] };
    array[0] = 5;
    delete[] array;
    return 0;
}