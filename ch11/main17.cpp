#include <iostream>
#include <string>
#include <string_view>

int main()
{
    constexpr std::string_view names[]{ "Alex",  "Betty", "Caroline", "Dave",
                                        "Emily", "Fred",  "Greg",     "Holly" };

    std::cout << "enter name: ";
    std::string uname{};
    std::cin >> uname;

    bool found{ false };

    for (std::string_view name : names)
    {
        if (name == uname)
        {
            found = true;
            break;
        }
    }

    if (found)
        std::cout << uname << " was found.\n";
    else
        std::cout << uname << " was not found.\n";

    return 0;
}