#include <iostream>
#include <iterator>

int main()
{
    int value{ 7 };
    int *ptr{ &value };

    std::cout << ptr << '\n';
    std::cout << ptr + 1 << '\n';
    std::cout << ptr + 2 << '\n';
    std::cout << ptr + 3 << '\n' << '\n';

    short sval{ 57 };
    short *sptr{ &sval };

    std::cout << sptr << '\n';
    std::cout << sptr + 1 << '\n';
    std::cout << sptr + 2 << '\n';
    std::cout << sptr + 3 << '\n' << '\n';

    int array[]{ 6, 4, 2, 9, 6, 3 };

    constexpr int len{ static_cast<int>(std::size(array)) };

    for (int i{0}; i < len; ++i)
    {
        std::cout << "element " << i << ": " << &(array[i]) << '\n';
    }

    return 0;
}