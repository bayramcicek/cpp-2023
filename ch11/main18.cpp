#include <iostream>

int main()
{
    int value{ 5 };
    int *ptr{ &value };
    int **ptrptr{ &ptr };

    //std::cout << **ptrptr << '\n';

    int **array{ new int *[10] }; // array of 10 pointers

    int x{ 7 };
    int(*abc)[2]{ new int[4][2] };

    ////////////////

    int my1[3][2]{ { 9, 8 }, { 7, 6 }, { 5, 4 } };
    int(*my2)[3][2]{ &my1 };
    int(**my3)[3][2]{ &my2 };

    std::cout << &my1 << ' ' << my1 << '\n';

    for (const auto &i : my1)
    {
        std::cout << &i << ' ' << i << '\n';
        std::cout << "-> " << *(i + 0) << ' ' << *(i + 1) << '\n';
    }

    return 0;
}