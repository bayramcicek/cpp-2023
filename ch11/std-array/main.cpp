#include <array>
#include <iostream>
#include <algorithm>

int main()
{
    std::array<int, 3> myArray;
    std::array myArray2{ 1, 2, 3, 45 };

    myArray.at(2) = 3;
    std::sort(myArray2.rbegin(), myArray2.rend());

    //std::cout << myArray2.size() << '\n';
    for (int i : myArray2)
    {
        std::cout << i << ' ';
    }
    return 0;
}