#include <array> 
#include <cstddef> // std::size_t
#include <iostream>

template <typename T, std::size_t size>
void printArray(const std::array<T, size> &myArray)
{
    for (auto i : myArray)
    {
        std::cout << i << ' ';
    }
    std::cout << '\n';
}

int main()
{
    std::array myArr{ 9.0, 7.2, 5.4, 3.6, 1.8 };
    printArray(myArr);

    for (std::size_t i{0}; i < myArr.size(); ++i)
    {
        std::cout << myArr[i] << ' ';
    }



    return 0;
}