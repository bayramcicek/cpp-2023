#include <iostream>

int main()
{
    int **array{ new int *[10] }; // rows

    for (int i{ 0 }; i < 10; ++i)
    {
        array[i] = new int[5]; // columns
    }
    array[4][7] = 56;
    (array[4])[8] = 99;

    std::cout << (array[4])[8] << '\n';

    for (int i{ 0 }; i < 10; ++i)
    {
        delete[] array[i];
    }

    delete[] array;




    return 0;
}