#include <iostream>

int main()
{
    int array[]{ 9, 7, 5, 3, 2 };
    std::cout << &(array[1]) << '\n';
    std::cout << array + 1 << '\n';

    std::cout << array[1] << '\n';
    std::cout << *(array + 1) << '\n';

    return 0;
}