#include <iostream>

int main()
{
    int prime[5]{};

    prime[0] = 2;

    std::cout << prime[0] << '\n';
    std::cout << prime[1] << '\n';

    int test[5]{ 1, 2, 3, 4, 5 };

    for (int i{ 0 }; i < 5; i++)
    {
        std::cout << test[i] << '\n';
    }

    int five[]{ 1, 2, 3, 4, 5 };

    return 0;
}