#include <iostream>

int main()
{
    const char *name1{ "Alex" };
    const char *name2{ "Alex" };
    char abc[]{ "my value" };

    std::cout << &name1 << '\n';
    std::cout << &name2 << '\n';
    name1 = "selam";

    std::cout << name1 << '\n';
    return 0;
}