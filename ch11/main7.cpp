#include <iostream>
#include <iomanip>

int main()
{
    int array[3][5]{ { 1, 2, 3, 4, 5 },
                     { 6, 7, 8, 9, 10 },
                     { 11, 12, 13, 14, 15 } };

    constexpr int numRows{ 11 };
    constexpr int numCols{ 11 };

    int product[numRows][numCols]{};

    for (int row{ 1 }; row < numRows; ++row)
    {
        for (int col{ 1 }; col < numCols; ++col)
        {
            product[row][col] = row * col;
        }
    }

    for (int i{ 1 }; i < numRows; ++i)
    {
        for (int j{ 1 }; j < numRows; ++j)
        {
            std::cout << std::setw(2) << product[i][j] << ' ';
        }
        std::cout << '\n';
    }

    return 0;
}