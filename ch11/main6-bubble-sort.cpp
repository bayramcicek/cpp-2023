#include <algorithm>
#include <iostream>
#include <iterator>

// bubble sort
int main()
{
    int array[]{ 6, 3, 2, 9, 7, 1, 5, 4, 8 };
    constexpr int length{ static_cast<int>(std::size(array)) };

    for (int i{ length - 2 }; i >= 0; --i)
    {
        bool swapped{ false };
        for (int j{ 0 }; j <= i; ++j)
        {
            if (array[j] > array[j + 1])
            {
                std::swap(array[j], array[j + 1]);
                swapped = true;
            }
        }

        if (!swapped)
        {
            std::cout << "Early termination on iteration: " << length - 1 - i
                      << '\n';
            break;
        }
    }

    for (int i{ 0 }; i < length; ++i)
    {
        std::cout << array[i] << ' ';
    }

    std::cout << '\n';

    return 0;
}