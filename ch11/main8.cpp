#include <iostream>
#include <iterator>

int main()
{
    char myStr[]{ "string" };
    const int length{ static_cast<int>(std::size(myStr)) };

    std::cout << length << '\n';

    for (int i{ 0 }; i < length; ++i)
        std::cout << static_cast<int>(myStr[i]) << ' ';

    std::cout << '\n';

    myStr[2] = 'o';
    std::cout << myStr[2] << '\n';

    return 0;
}