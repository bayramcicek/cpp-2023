#include <iostream>
#include <iterator>

namespace StudentNames
{
enum StudentNames
{
    kenny, // 0
    kyle, // 1
    stan, // 2
    butters, // 3
    cartman, // 4
    wendy, // 5
    max_students // 6
};
}

int main()
{
    int testScores[StudentNames::max_students]{};
    testScores[StudentNames::kyle] = 76;

    std::cout << std::size(testScores) << '\n';
    return 0;
}