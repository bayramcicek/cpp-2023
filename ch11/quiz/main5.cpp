#include <iostream>

void printChar(const char *str)
{
    while (*str != '\0')
    {
        std::cout << *str << '\n';
        ++str;
    }
}

int main()
{
    const char *str{ "Hello, world" };

    printChar(str);

    return 0;
}