#include <algorithm>
#include <iostream>
#include <iterator>

int *findValue(int *begin, int *last, int value)
{
    for (int *i{ begin }; i != last; ++i)
    {
        if (*i == value)
        {
            return i;
        }
    }
    return last;
}

int main()
{
    int arr[]{ 2, 5, 4, 10, 8, 20, 16, 40 };

    // Search for the first element with value 20.
    int *found{ findValue(std::begin(arr), std::end(arr), 20) };

    // If an element with value 20 was found, print it.
    if (found != std::end(arr))
    {
        std::cout << *found << '\n';
    }

    auto found_new{ std::find(std::begin(arr), std::end(arr), 20) };

    if (found_new != std::end(arr))
    {
        std::cout << *found_new << '\n';
    }

    return 0;
}