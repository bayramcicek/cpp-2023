#include <iostream>
#include <vector>

int main()
{
    constexpr int fibonacci[]{ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };

    for (auto i : fibonacci)
    {
        std::cout << i << ' ';
    }
    std::cout << '\n';

    std::string array[]{ "peter", "likes", "frozen", "yogurt" };
    for (const auto &element : array)
    {
        std::cout << element << ' ';
    }

    std::cout << '\n';

    std::vector vfib{ 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89 };

    for (int j{ 0 }; auto i : vfib)
    {
        std::cout << j << ':' << i << '\n';
        ++j;
    }

    std::cout << '\n';

    return 0;
}