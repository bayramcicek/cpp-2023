#include <iostream>
#include <vector>

void pLength(const std::vector<int> &v)
{
    std::cout << "length: " << v.size() << '\n';
}

int main()
{
    std::vector<int> v1{ 1, 2, 3, 4 };
    std::vector v2{ 2, 3, 4, 4 };
    v1[3] = 34;
    v2.at(1) = 66;
    v2 = { 3, 4, 5, 6, 7, 6, 54, 34, 345, 45 };

    pLength(v2);

    return 0;
}