#include <iostream>
#include <vector>

int main()
{
    std::vector<int> v{ 0, 1, 2 };
    std::cout << v.size() << '\n'; // 3
    v.resize(5);
    std::cout << v.size() << '\n'; // 5

    std::vector<int> b(4, 23);
    for (int i : b)
    {
        std::cout << i << ' ';
    }
    std::cout << '\n';

    std::vector<bool> v2{ true, false, false, true, true };
    std::cout << "The length is: " << v2.size() << '\n';

    for (int i : v2)
        std::cout << i << ' ';

    std::cout << '\n';

    return 0;
}