#include <iostream>
#include <string>

class Person
{
public:
    std::string m_name{};
    int m_age{};

    Person(const std::string &name = "", int age = 0)
        : m_name{ name }
        , m_age{ age }
    {
    }

    const std::string &getName() const { return m_name; }
    int getAge() const { return m_age; }
};

class Employee : public Person
{
public:
    double m_hourlySalary{};
    long m_employeeID{};

    Employee(double hourlySalary = 0.0, long employeeID = 0)
        : m_hourlySalary{ hourlySalary }
        , m_employeeID{ employeeID }
    {
    }

    void printNameAndSalary() const
    {
        std::cout << m_name << ": " << m_hourlySalary << '\n';
    }
};

class BaseballPlayer : public Person
{
public:
    double m_battingAverage{};
    int m_homeRuns{};

    BaseballPlayer(double battingAverage = 0.0, int homeRuns = 0)
        : m_battingAverage{ battingAverage }
        , m_homeRuns{ homeRuns }
    {
    }
};

int main()
{
    // BaseballPlayer bayram{};
    // bayram.m_name = "Bayram";
    // std::cout << bayram.getName() << '\n';

    Employee frank{ 20.25, 123456 };
    frank.m_name = "Frank";
    frank.printNameAndSalary();

    return 0;
}