#include <iostream>
#include <string>

int main()
{
    std::cout << "pick 1 or 2: ";
    int choice {};
    std::cin >> choice;

    std::cout << "name: ";
    std::string name{};
    std::getline(std::cin >> std::ws, name);

    std::cout << "hello " << name << '\n';

    return 0;
}