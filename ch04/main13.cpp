#include <iostream>
#include <string>

int main()
{
    std::string name { "Alex" };
    std::cout << name.length() << '\n'; // unsigned
    int length = static_cast<int>(name.length());
    std::cout << length << '\n';
    return 0;
}