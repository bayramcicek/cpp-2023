#include <iostream>
#include <string_view>

void printSV(std::string_view str)
{
    std::cout << str << '\n';
}

int main()
{
    constexpr std::string_view s{ "test 123" };
    printSV(s);
    return 0;
}