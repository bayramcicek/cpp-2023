#include <iostream>
#include <iomanip>

int main()
{
    std::int32_t
        myVar{}; // insertion operator (<<) , extraction operator (>>) , semicolon (;)
   // std::cout << sizeof(std::size_t) << '\n'; // 4 byte
   // std::cout << sizeof(long double) << '\n'; // 16 byte

    double y{ 5.0 };
    float z{ 7.2f };

    std::cout << 5.0 << '\n'; // 5
    std::cout << 4.2f << '\n'; // 4.2
    std::cout << 9876543.21 << '\n'; // 9.87654e+06

    std::cout << 7.123456789f << '\n';

    double d{ 0.1 };
    std::cout << std::setprecision(17);
    std::cout << d << '\n';

    double d2{ 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 }; // should equal 1.0
    std::cout << d2 << '\n'; // 0.99999999999999989

    double zero {0.0};
    double posinf { 5.0 / zero }; // positive infinity
    std::cout << posinf << '\n';

    double neginf { -5.0 / zero }; // negative infinity
    std::cout << neginf << '\n';

    double nan { zero / zero }; // not a number (mathematically invalid)
    std::cout << nan << '\n';

    return 0;
}