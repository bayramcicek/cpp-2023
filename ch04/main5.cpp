#include <iostream>

int main()
{
    // false -> 0
    // true -> 1
    bool isTrue { !true };

    std::cout << std::boolalpha;
    std::cout << isTrue << '\n'; // false

    std::cout << std::noboolalpha;
    std::cout << isTrue << '\n'; // 0

    bool a = 45;
    std::cout << a << '\n';

    bool b = 0;
    std::cout << b << '\n';

    return 0;
}