#include <iostream>

double getNumber()
{
    std::cout << "Enter a double value: ";
    double num{};
    std::cin >> num;

    return num;
}

char getOperation()
{
    std::cout << "Enter one of the following: +, -, *, or / : ";
    char op{};
    std::cin >> op;
    return op;
}

void printResult(double x, char op, double y)
{
    if (op == '+')
        std::cout << "res: " << x + y << '\n';
    else if (op == '-')
        std::cout << "res: " << x - y << '\n';
    else if (op == '*')
        std::cout << "res: " << x * y << '\n';
    else if (op == '/')
        std::cout << "res: " << x / y << '\n';
}

int main()
{
    double first{ getNumber() };
    double second{ getNumber() };

    char operation{ getOperation() };

    printResult(first, operation, second);

    return 0;
}