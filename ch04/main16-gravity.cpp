#include <iostream>

double calculateHeight(double towerHeight, int seconds)
{
    constexpr double gravity{ 9.8 };
    const double distanceFallen{ (gravity * (seconds * seconds)) / 2 };
    const double currentHeight{ towerHeight - distanceFallen };

    return currentHeight;
}

int main()
{
    std::cout << "Enter the height of the tower in meters: ";
    double meter{};
    std::cin >> meter;

    for (int i = 0;; i++)
    {
        double x{ calculateHeight(meter, i) };
        if (x < 0)
        {
            std::cout << "At " << i << " seconds, the ball is on the ground.\n";
            break;
        }
        else
        {
            std::cout << "At " << i << " seconds, the ball is at height: "
            << x << '\n';
        }
    }

    return 0;
}