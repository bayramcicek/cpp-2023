#include <bitset>
#include <iostream>

int main()
{
    double avogadro{ 6.02e73 }; // 6.02e+73
    std::cout << avogadro << '\n';

    int x{ 025 }; // 0 before the number means this is octal(base 8)
    std::cout << x << '\n'; // 21 (output is decimal by default)

    int y{ 0x2C }; // hex
    std::cout << y << '\n'; // 44

    int bin{ 0b0000'0101 };
    std::cout << bin << '\n';

    int num{ 12 };
    std::cout << num << '\n';
    std::cout << std::hex << num << '\n';
    std::cout << std::oct << num << '\n';
    std::cout << num << '\n'; // octal
    std::cout << std::dec << num << '\n'; // 12

    // bitset
    std::bitset<8> bin1{ 0b1100'0101 }; // 11000101
    std::bitset<8> bin2{ 0xC5 }; // 11000101

    std::cout << bin1 << '\n' << bin2 << '\n';
    std::cout << std::bitset<4>(0b0110) << '\n';
    std::cout << std::bitset<4>{ 0b0110 } << '\n';

    return 0;
}