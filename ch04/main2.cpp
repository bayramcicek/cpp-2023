#include <iostream>

int main()
{
    unsigned short x{ 65535 };
    std::cout << "x was: " << x << '\n';

    x = x + 1;
    std::cout << "x now: " << x << '\n';

    x = x + 1;
    std::cout << "x was: " << x << '\n';

    signed int s {-1};
    unsigned int u {1};

    if (s < u) // -1 is implicitly converted to 4294967295, and 4294967295 < 1 is false
    {
        std::cout << "less than\n";
    } else std::cout << "greater than\n"; // this executes

    return 0;
}