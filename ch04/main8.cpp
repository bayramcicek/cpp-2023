#include <iostream>
#define MAX_STUDENTS 30

void printInt(const int x)
{
    std::cout << x << '\n';
}

int main()
{
    const double gravity { 9.8 };
    std::cout << "gravity: " << gravity << '\n';
    printInt(44);
    std::cout << "max: " << MAX_STUDENTS << '\n';

    return 0;
}