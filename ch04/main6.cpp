#include <iostream>
#include <cstdint>

void print(int d)
{
    std::cout << d << '\n';
}

int main()
{
    print( static_cast<int>( 7.3 ) );

    char ch{ 97 }; // a
    std::cout << ch << '\n'; // a
    std::cout << static_cast<int>(ch) << '\n'; // 97

    unsigned int u { 5u };
    int s { static_cast<int>(u) };
    std::cout << s << '\n';
    std::int8_t myInt { 65 };

    std::cout << static_cast<int>(myInt) << '\n';

    return 0;
}