#include <iostream>

int main()
{
    char myChar {};
    std::cout << "enter a single char: ";
    std::cin >> myChar;

    std::cout << "you entered: " << myChar << ", has ASCII code: " 
    << static_cast<int>(myChar)
    << '\n';

    return 0;
}