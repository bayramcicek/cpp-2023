#include <iostream>

int five()
{
    return 5;
}

int main()
{
    constexpr double gravity { 9.8 };
    constexpr int sum { 4 + 5 };

    std::cout << "enter your age: ";
    int age {};
    std::cin >> age;

    // constexpr int myAge { age }; // compile error: age is not a constant expression
    return 0;
}