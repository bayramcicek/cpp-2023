#include <iostream>

bool isEven(int num) { return (num % 2 == 0); }

int main()
{
    std::cout << "Enter an integer: ";
    int number{};
    std::cin >> number;

    std::cout << number << (isEven(number) ? (" is even") : (" is odd"))
              << '\n';

    return 0;
}