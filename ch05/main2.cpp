#include <iostream>

int add(int x, int y)
{
    return x + y;
}

int main()
{
    int x{ 1 };
    // int value{ add(x, ++x) };
    // std::cout << value << '\n'; // 6 + 6 = 12
    int y{ 2 };

    std::cout << (++x, ++y) << '\n';


    return 0;
}