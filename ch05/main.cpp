#include <iostream>
#include <cmath>

int main()
{
    // std::cout << 34 / 0 << '\n'; // Floating point exception (core dumped)
    std::cout << 34.8 / 0.0 << '\n'; // inf

    std::cout << std::pow(2.0, 3.0) << std::endl; // 8

    return 0;
}