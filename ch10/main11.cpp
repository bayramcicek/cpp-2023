#include <iostream>
#include <utility>

template <typename T, typename U>
void print(std::pair<T, U> p)
{
    std::cout << '[' << p.first << ", " << p.second << ']' << '\n';
}

int main()
{
    std::pair<int, double> p1{ 23, 99.4 };
    print(p1);

    return 0;
}