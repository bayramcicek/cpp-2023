#include <iostream>

enum Levels
{
    beginner,
    intermediate,
    advanced
};

void rType(Levels level)
{
    if (level == intermediate)
    {
        std::cout << "keep going" << '\n';
    }
    else
        std::cout << "up" << '\n';
}

int main()
{
    rType(Levels::advanced);

    Levels beg{ beginner };

    if (beg == beginner)
    {
        std::cout << "yes" << '\n';
    }

    std::cout << "enter 1, 2, 3: ";
    int input{};
    std::cin >> input;

    Levels lev1{ static_cast<Levels>(input) };
    

    return 0;
}