#include <iostream>

// type definition
struct Employee
{
    int id{};
    int age{};
    double wage{};
};

int main()
{
    Employee bayram;
    bayram.age = 25;

    std::cout << bayram.age << '\n';
    return 0;
}