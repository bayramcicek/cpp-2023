#include <iostream>

struct Paw
{
    int claws{};
};

struct Animal
{
    std::string name{};
    Paw paw{};
};

int main()
{
    Animal puma{ "Puma", { 5 } };

    Animal *ptr{ &puma };

    std::cout << (ptr->paw).claws << '\n';
    std::cout << ptr->paw.claws << '\n';
    // operator-> and operator. evaluate in left to right order

    return 0;
}