#include <iostream>

enum class Animal
{
    pig,
    chicken,
    goat,
    cat,
    dog,
    duck
};

constexpr std::string_view getAnimalName(Animal a)
{
    switch (a)
    {
        case Animal::pig:
            return "pig";
        case Animal::chicken:
            return "chicken";
        case Animal::goat:
            return "goat";
        case Animal::cat:
            return "cat";
        case Animal::dog:
            return "dog";
        case Animal::duck:
            return "duck";
        default:
            return "???";
    }
}



int main() { return 0; }