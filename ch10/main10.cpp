#include <iostream>

template <typename T>
struct Pair
{
    T first{};
    T second{};
};

template <typename T>
constexpr T max(Pair<T> p)
{
    return (p.first > p.second ? p.first : p.second);
}

int main()
{
    Pair<int> p1{ 4, 6 };
    std::cout << p1.first << ' ' << p1.second << '\n';

    Pair<double> d1{ 3.4, 7.1 };
    std::cout << d1.first << ' ' << d1.second 
    << '\n' << max<int>(p1)
    <<'\n';

    return 0;
}