#include <iostream>

// type definition
struct Employee
{
    int id{};
    int age{};
    double wage{};
};

int main()
{
    Employee bayram{ 1, 25, 2500 };

    bayram = { bayram.id, 26, 3500 };

    std::cout << bayram.id << '\n';
    std::cout << bayram.age << '\n';
    std::cout << bayram.wage << '\n';

    // bayram.id = 2;
    return 0;
}