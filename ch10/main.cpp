#include <iostream>

enum Color
{
    red,
    green,
    blue
};

int main()
{
    Color apple{ red };
    Color phone{ blue };

    std::cout << phone << '\n'; // 2

    return 0;
}