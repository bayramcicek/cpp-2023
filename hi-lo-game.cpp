#include <iostream>

// get random number [1-100]
int getRandomNumber()
{
    srand(time(NULL));
    int random = 1 + (rand() % 100);
    return random;
}

int main()
{
    std::cout <<    "<-----Welcome to HI-LO Game------>\n" <<
                    "<-----Your computer kept a number between [1-100] in its memory!------->\n";

    int number = getRandomNumber();
    bool isGuessed = false;

    while (!isGuessed)
    {
        // TO-DO: add error handling
        std::cout << "> It's your turn - Guess the number: ";
        int userNumber { 0 };
        std::cin >> userNumber;

        if (userNumber > number)
        {
            std::cout << ">> Too high\n";
        }
        else if (userNumber < number)
        {
            std::cout << ">> Too low\n";
        }
        else
        {
            std::cout << ">> Correct! - " << userNumber << '\n';
            isGuessed = true;
        }
    }

    return 0;
}