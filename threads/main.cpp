#include <iostream>
#include <thread>

static bool s_Finished{ false };

void DoWork()
{
    using namespace std::literals::chrono_literals;
    std::cout << "started thread id: " << std::this_thread::get_id() << '\n';
    while (!s_Finished)
    {
        std::cout << "Working...\n";
        std::this_thread::sleep_for(1s);
    }
}

int main()
{
    std::thread worker(DoWork);
    std::cout << "started thread id: " << std::this_thread::get_id() << '\n';

    std::cin.get();
    s_Finished = true;

    worker.join();
    std::cout << "Finished" << '\n';
    std::cout << "started thread id: " << std::this_thread::get_id() << '\n';
    std::cin.get();

    return 0;
}