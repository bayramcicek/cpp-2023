#include <chrono>
#include <iostream>
#include <map>
#include <string>
#include <thread>
#include <functional>

void refreshForecast(std::map<std::string, int> &fm)
{
    using namespace std::chrono_literals;
    while (true)
    {
        for (auto &item : fm)
        {
            item.second++;
            std::cout << item.second << '\n';
        }

        std::this_thread::sleep_for(2000ms);
        break;
    }
}

int main()
{
    std::map<std::string, int> forecastMap
        = { { "New York", 15 }, { "Mumbai", 33 }, { "Turkiye", 15 } };

    std::thread bgWorker(refreshForecast, std::ref(forecastMap));

    bgWorker.join();

    for (auto &i : forecastMap)
    {
        std::cout << i.second << ' ';
    }

    return 0;
}