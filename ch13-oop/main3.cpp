#include <iostream>

class DateClass
{
public:
    int m_month{};
    int m_day{};
    int m_year{};
};

int main()
{
    DateClass date;
    date.m_month = 10;

    std::cout << date.m_month << '\n';
    std::cout << date.m_day << '\n';

    return 0;
}