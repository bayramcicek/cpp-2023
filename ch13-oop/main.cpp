#include <iostream>

struct DateStruct
{
    int year{};
    int month{};
    int day{};
};

class DateClass
{
public:
    int m_year{};
    int m_month{};
    int m_day{};

    void print()
    {
        std::cout << m_year << '/' << m_month << '/' << m_day << '\n';
    }
};

int main()
{
    DateClass today{ 2023, 1, 23 };
    today.m_day = 24;

    std::cout << today.m_year << '\n'; // 2023
    today.print();

    return 0;
}