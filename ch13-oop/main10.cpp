#include <iostream>

class Something
{
private:
    int m_value1{};
    double m_value2{};
    const int m_const{}; // must be initialized
    char m_value3{};

public:
    Something(int value1, double value2, int val, char value3 = 'c')
        : m_value1{ value1 }
        , m_value2{ value2 }
        , m_const{ val }
        , m_value3{ value3 }
    {
    }

    void print()
    {
        std::cout << "Something(" << m_value1 << ", " << m_value2 << ", "
                  << m_const << ", " << m_value3 << ")\n";
    }
};

int main()
{
    Something something{ 1, 2.2 , 42};
    something.print();

    return 0;
}