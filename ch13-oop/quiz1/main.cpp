#include <cmath>
#include <iostream>

class Point2d
{
private:
    double m_x{};
    double m_y{};

public:
    Point2d(double x = 0.0, double y = 0.0)
        : m_x{ x }
        , m_y{ y }
    {
    }

    void print() const
    {
        std::cout << "Point2d(" << (*this).m_x << ", " << this->m_y << ")\n";
    }

    double distanceTo(const Point2d &p) const
    {
        double x{ this->m_x - p.m_x };
        double y{ this->m_y - p.m_y };
        return std::sqrt(x * x + y * y);
    }
};

int main()
{
    Point2d first{};
    Point2d second{ 3.0, 4.0 };
    first.print();
    second.print();
    std::cout << "Distance between two points: " << first.distanceTo(second)
              << '\n';  

    return 0;
}