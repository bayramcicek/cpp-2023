#include <iostream>
#include <sys/random.h>
#include <vector>

#define TEST 0
int main()
{
#if TEST == 1
    std::vector<char> random{ 'w' };
    const std::size_t length = 16;
    int fd = open("/dev/urandom", O_RDONLY);

    int len = 0;
    std::cout << fd << ' ' << len << ' ' << random.data() << '\n';

    if (fd < 0 || (len = read(fd, random.data(), length)) < 0
        || std::size_t(len) < length)
    {
        std::cerr << "failed to read " << length << " hard random bytes, got "
                  << len << " for hash: " << errno;
    }

    std::cout << fd << ' ' << len << ' ' << random.data() << '\n';
    // std::cout << random.at(0) << '\n';
    close(fd);

#elif TEST == 2
    std::vector<char> buffer(1);
    const std::size_t length = 8;
    int len = 0;

    std::cout << len << ' ' << length << ' ' << buffer.data() << '\n';

    len = getrandom(buffer.data(), length, GRND_NONBLOCK);

    std::cout << len << ' ' << length << ' ' << buffer.data() << '\n';

#else
    char buffer[8];
    int a = getrandom(buffer, sizeof(buffer), GRND_NONBLOCK);
    std::cout << sizeof(buffer) << ' ' << a << '\n';
#endif

    return 0;
}