// https://riptutorial.com/cplusplus/topic/699/threading
#include <iostream>
#include <thread>

void foo(int a)
{
    std::cout << a << '\n';
}

int main()
{
    std::thread tWorker(foo, 10);
    tWorker.join();
    
    return 0;
}