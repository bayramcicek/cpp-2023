#include <iostream>

#define PRINT  // PRINT is only #defined from the point of definition to the end of main.cpp

void doSomething(); // forward declaration

int main()
{

    doSomething();
    return 0;
}