#include <iostream>

int main() {
    std::cout << "hello world - all over again!" << std::endl;
    int a {5};
    std::cout << a << '\n'; // insertion operator (<<)
    std::cout << "insert a number: ";
    int reply {};
    std::cin >> reply; // extraction operator (>>)
    std::cout << reply << '\n';

    std::cout << "enter two number: ";
    int f1 {};
    int f2 {};
    std::cin >> f1 >> f2;
    std::cout << "entered: " << f1 << " and: " << f2 << '\n';

    return 0;
}
