#include "square.h"
#include <iostream>

int main()
{
    std::cout << "header files" << '\n';
    std::cout << "a square has " << getSquareSides() << " sides\n";
    return 0;
}