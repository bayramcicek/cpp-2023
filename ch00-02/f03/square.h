#ifndef SQUARE_H
#define SQUARE_H

int getSquareSides(); // forward declaration
int getSquarePerimeter(int sideLength);

#endif