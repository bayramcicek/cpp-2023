#include <iostream>

int add(int a, int b); // forward declaration

int main () {
    
    std::cout << "sum: " << add(3, 4) << '\n';

    return 0;
}

int add(int a, int b)
{
    return a + b;
}
