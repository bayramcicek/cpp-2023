#include <iostream>

void testFunction() { std::cout << "test func\n"; }

int returnAValue()
{
    std::cout << "enter an int: ";
    int foo{};
    std::cin >> foo;

    return foo;
}

int main()
{
    std::cout << "test \n";
    testFunction();

    int x{ returnAValue() };

    std::cout << "you entered: " << x << '\n';

    return 0;
}