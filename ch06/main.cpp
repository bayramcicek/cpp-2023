#include <iostream>

namespace foo
{
    int doSomething(int x, int y)
    {
        return x + y;
    }
}

namespace goo
{
    int doSomething(int x, int y)
    {
        return x - y;
    }
}

int main()
{
    std::cout << foo::doSomething(2, 3) << '\n';
    std::cout << goo::doSomething(2, 3) << '\n';

    int a {8};
    {
        int a {9};
    }
    return 0;
}