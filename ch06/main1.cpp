#include <iostream>

extern int g_x;
extern const int g_y;

int main()
{
    std::cout << g_x << '\n'; // 2
    std::cout << g_y << '\n'; // 3

    return 0;
}