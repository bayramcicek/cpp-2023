#include "constants.h"
#include <iostream>

int main()
{
    std::cout << "enter a radius: ";
    int radius{};
    std::cin >> radius;

    std::cout << "the circumference is: " << 2.0 * radius * constants::pi << '\n';

    return 0;
}