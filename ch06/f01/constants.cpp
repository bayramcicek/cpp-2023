#include "constants.h"

namespace constants
{
extern const double pi{ 3.14159 };
extern const double avogadro{ 6.0221413e23 };

// m/s^2 -- gravity is light on this planet
extern const double myGravity{ 9.2 };
}