#include <iostream>

int main()
{
    int outer{ 1 };

    while (outer <= 5)
    {
        int inner{ 5 };
        while (inner >= 1)
        {
            if (inner > outer)
            {
                std::cout << "X ";
            }
            else
            {
                std::cout << inner << ' ';
            }
            
            --inner;
        }

        std::cout << '\n';
        ++outer;
    }
    
    return 0;
}

/*
X X X X 1 
X X X 2 1 
X X 3 2 1 
X 4 3 2 1 
5 4 3 2 1 */