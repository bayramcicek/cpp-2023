#include <iostream>

int main()
{
    double x{};
tryAgain:
    std::cout << "Enter a negative number: ";
    std::cin >> x;

    if (x < 0.0)
        goto tryAgain;
    
    std::cout << "number is: " << x << '\n';

    return 0;
}