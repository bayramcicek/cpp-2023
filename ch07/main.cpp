#include <iostream>

int calculate(int x, int y, char z)
{
    switch(z)
    {
        case '+':
            return x + y;
        case '-':
            return x - y;
        case '*':
            return x * y;
        case '/':
            return x / y;
        case '%':
            return x % y;
        default:
            std::cerr << "an error occured\n";
            return 0;
    }
}

int main()
{
    int a = calculate(24, 8, '/');
    std::cout << a << '\n';

    return 0;
}