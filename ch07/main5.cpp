#include <iostream>
#include <cassert>
#include <cmath>

double calculateTimeUntilObjectHitsGround(double initialHeight, double gravity)
{
    assert((gravity > 0.0) && "gravity is below zero");

    if (initialHeight <= 0.0)
    {
        return 0.0;
    }

    return std::sqrt((2.0 * initialHeight) / gravity);
}

int main()
{
    std::cout << "took " << calculateTimeUntilObjectHitsGround(100.0, -9.8) << " seconds\n";
    return 0;
}