#include <iostream>
#include <string>

int main()
{
    int x{ 4 };
    std::string sFour{ std::to_string(x) };
    std::string sSixPointSeven{ std::to_string(6.7) };
    std::string sA{ 'A' };

    std::cout << sFour << '\n' << sSixPointSeven << '\n' << sA << '\n';
    std::cout << std::stod(sSixPointSeven) << '\n';
    std::string s{"MyString"};
    std::cout << s.max_size() << '\n';
    return 0;
}