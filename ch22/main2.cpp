#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>

int main()
{
    std::string abc{ "01234567" };
    std::cout << "Length: " << abc.length() << '\n';
    std::cout << "Capacity: " << abc.capacity() << '\n';

    std::srand(std::time(nullptr));
    std::string s{};
    s.reserve(64);

    for (int count{ 0 }; count < 64; ++count)
    {
        s += 'a' + std::rand() % 26;
    }

    std::cout << s << '\n';

    return 0;
}