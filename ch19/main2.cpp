#include <iostream>

template <typename T> class Storage
{
private:
    T m_value{};

public:
    Storage(T value)
        : m_value{ value }
    {
    }

    void print() { std::cout << m_value << '\n'; }
};

template <> void Storage<double>::print()
{
    std::cout << std::scientific << m_value << '\n';
}

int main()
{
    Storage<int> iValue{ 5 };
    Storage<double> dValue{ 2.8 };

    iValue.print();
    dValue.print();

    return 0;
}