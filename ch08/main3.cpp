#include <iostream>

template <typename U, typename Z>
auto min(U x, Z y)
{
    return (x < y) ? x : y;
}


template <typename T>
T max(T x, T y)
{
    return (x > y) ? x : y;
}

int max(int a, int b)
{
    std::cout << "int max() called ";
    return 0; 
    
}

int main()
{
    std::cout << max<int>(5, 8) << '\n'; // 8
    std::cout << max<>(5.6, 5.1) << '\n'; // 5.6
    std::cout << max(1, 2) << '\n'; // 2
    std::cout << min<int, double>(3, 7.8) << '\n';

    return 0;
}