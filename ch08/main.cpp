#include <iostream>
#include <typeinfo>

auto foo();

int main()
{
    std::cout << "mutlu yıllar" << '\n';
    int i{ 2 };
    double d{ 3.5 };
    std::cout << typeid(i + d).name() << ' ' << i + d << '\n';
    // d 5.5

    short a{ 4 };
    short b{ 5 };
    std::cout << typeid(a + b).name() << ' ' << a + b << '\n';
    // i 9

    using Distance = double;
    Distance miles{ 3.5 };
    std::cout << miles << '\n';

    const int x{ 4 };
    auto y{ x }; // const dropped
    const auto z{ x }; // z will be type const int

    // foo(); // error: use of ‘auto foo()’ before deduction of ‘auto’

    return 0;
}

auto foo()
{
    return 2;
}