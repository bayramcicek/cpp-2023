#include <iostream>

int foo() // code starts at memory address 0x002717f0
{
    std::cout << "foo -->" << '\n';
    return 5;
}

int main()
{
    std::cout << reinterpret_cast<void*>(foo) << '\n';
    int (*fcnPtr)(){ &foo };
    std::cout << reinterpret_cast<void*>(foo) << '\n';
    
    foo();
    (*foo)();

    return 0;
}