#include <iostream>
#include <vector>

int fib(int count)
{
    static std::vector results{ 0, 1 };

    if (count < static_cast<int>(std::size(results)))
        return results[count];

    results.push_back(fib(count - 1) + fib(count - 2));
    return results[count];
}

int main()
{
    for (int count{ 0 }; count < 13; ++count)
    {
        std::cout << fib(count) << ' ';
    }
    return 0;
}