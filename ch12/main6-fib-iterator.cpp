#include <iostream>

int main()
{
    int result{ 0 };
    int val{ 1 };
    int count{ 10 };

    // first 10 fibonacci numbers
    for (int i{ 0 }; i < count; ++i)
    {
        std::cout << result << ' ';

        int temp{ result };
        result += val;
        val = temp;
    }

    std::cout << '\n';

    return 0;
}