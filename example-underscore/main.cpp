#include <iostream>
#include <algorithm>
#include <string>

int main()
{
    std::string str("_my_ _underscore sour_ce");
    str.erase(std::remove(str.begin(), str.end(), '_'), str.end());

    std::cout << str << '\n';

    return 0;
}