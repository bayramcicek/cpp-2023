// C++ Threading #5: Unique Lock and Lazy Initialization
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

class LogFile
{
private:
    std::mutex _mu;
    std::ofstream _f{};
    std::once_flag _flag;

public:
    LogFile() {}
    void shared_print(std::string id, int value)
    {
        std::call_once(_flag, [&]() { _f.open("log.txt"); });
        std::unique_lock<std::mutex> locker(_mu, std::defer_lock);
        _f << "From " << id << "; " << value << '\n';
    }
};

int main() { return 0; }