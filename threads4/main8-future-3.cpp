// C++ Threading #7: Future, Promise and async()
#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>
#include <thread>

int factorial(std::shared_future<int> f)
{
    int res = 1;

    int n = f.get();

    for (int i = n; i > 1; --i)
    {
        res *= i;
    }

    std::cout << "result is: " << res << '\n';
    return res;
}

int main()
{
    int x{};

    std::promise<int> p;
    std::future<int> f = p.get_future();
    std::shared_future<int> sf = f.share();

    std::future<int> fu{ std::async(std::launch::async, factorial,
                                    sf) }; // async will create a thread
    std::future<int> fu2{ std::async(std::launch::async, factorial,
                                     sf) }; // async will create a thread
    std::future<int> fu3{ std::async(std::launch::async, factorial,
                                     sf) }; // async will create a thread

    p.set_value(4); // f.get() will send the value 4

    return 0;
}

/*
result is: 24
get from child: 24*/