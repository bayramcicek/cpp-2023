// C++ Threading #7: Future, Promise and async()
#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>
#include <thread>

int factorial(std::future<int> &f)
{
    int res = 1;

    int n = f.get();

    for (int i = n; i > 1; --i)
    {
        res *= i;
    }

    std::cout << "result is: " << res << '\n';
    return res;
}

int main()
{
    int x{};

    std::promise<int> p;
    std::future<int> f = p.get_future();

    // std::future<int> fu{ std::async(factorial, 4) }; // async may or may not create a thread
    // std::future<int> fu{ std::async(std::launch::deferred, factorial, 4) }; // async will not create a thread
    std::future<int> fu{
        std::async(std::launch::async, factorial, std::ref(f))
    }; // async will create a thread

    // do something else
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
    p.set_value(4); // f.get() will send the value 4

    x = fu.get(); // will wait until the child thread finish.
    // int ax = fu.get(); // crash

    std::cout << "get from child: " << x << '\n';

    return 0;
}

/*
result is: 24
get from child: 24*/