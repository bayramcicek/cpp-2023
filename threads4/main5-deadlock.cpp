#include <iostream>
#include <mutex>
#include <string>
#include <thread>
#include <fstream>


class LogFile
{
private:
    std::mutex _mu;
    std::mutex _mu2;
    std::ofstream _f{};
public:
    LogFile()
    {
        _f.open("log.txt");
    }
    void shared_print(std::string id, int value)
    {
        std::lock(_mu, _mu2);
        std::lock_guard<std::mutex> locker(_mu, std::adopt_lock);
        std::lock_guard<std::mutex> locker2(_mu2, std::adopt_lock);
        std::cout << "From " << id << "; " << value << '\n';
    }
    void shared_print2(std::string id, int value)
    {
        std::lock(_mu, _mu2);
        std::lock_guard<std::mutex> locker(_mu, std::adopt_lock);
        std::lock_guard<std::mutex> locker2(_mu2, std::adopt_lock);
        std::cout << "From " << id << "; " << value << '\n';
    }
};

void function_1(LogFile& log) 
{
    for (int i = 0; i >-100; --i)
    {
        /* code */
        log.shared_print("t1", i);
    }
    
}

int main()
{
    LogFile log;

    std::thread t1(function_1, std::ref(log));

    for (int i = 0; i < 100; ++i)
    {
        /* code */
        log.shared_print2(std::string("from main: "), i);
    }
    t1.join();

    /*
    [cicek@manjaro-21-2-2 build]$ /home/cicek/Documents/cpp-2023/build/run
From from main: ; 0
From t1; 0
From t1; -1
From t1; -2
From t1; -3
From t1; -4
From t1; -5
From t1; -6
From t1; -7
From t1; -8
From t1; -9
From t1; -10
From t1; -11
From t1; -12
From t1; -13
From t1; -14
From t1; -15
From t1; -16
From t1; -17
From t1; -18
From t1; -19
From t1; -20
From t1; -21
From t1; -22
From t1; -23
From t1; -24
From t1; -25
From from main: ; 1

<deadlock occured>

*/

/*
avoiding deadlock:
- prefer locking single mutex.
- avoid locking a mutex and then callşing a user provided function.
- use std::lock() to lock more than one mutex.
- lock the mutex in the same order.

locking granularity:
- fine-grained lock: protects small amount of data.
- coarse-grained lock: protects big amount of data.

*/
    
    return 0;
}
