// C++ Threading #9: packaged_task

#include <condition_variable>
#include <deque>
#include <functional>
#include <future>
#include <iostream>
#include <mutex>
#include <thread>

int factorial(int n)
{
    int res = 1;
    for (int i = n; i > 1; --i)
        res *= i;

    std::cout << "result is: " << res << '\n';
    return res;
}

std::deque<std::packaged_task<int()>> task_q;
std::mutex mu;
std::condition_variable cond;

void thread_1()
{
    std::packaged_task<int()> t;
    {
        std::unique_lock<std::mutex> locker(mu);
        cond.wait(locker, []() { return !task_q.empty(); });
        t = std::move(task_q.front());
        task_q.pop_front();
    }
    t();
}

int main()
{
    std::thread t1(thread_1);
    std::packaged_task<int()> t(std::bind(factorial, 6));
    std::future<int> fu = t.get_future();
    {
        std::lock_guard<std::mutex> locker(mu);
        task_q.push_back(std::move(t));
    }
    cond.notify_one();

    std::cout << fu.get() << '\n';
    t1.join();
    return 0;
}

/* summary
3 ways to get a future
- promise::get_future()
- packaged_task::get_future()
- async() returns a future
*/