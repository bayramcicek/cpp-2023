#include <iostream>
#include <mutex>
#include <string>
#include <thread>

std::mutex mu;

void shared_print(std::string msg, int id)
{
    std::lock_guard<std::mutex> guarg(mu); // RAII
    // mu.lock();
    std::cout << msg << ' ' << id << '\n';
    // mu.unlock();
}

void function_1()
{
    for (int i{ 0 }; i > -100; --i)
    {
        shared_print(std::string("From t1: "), i);
    }
}

int main() { 
    
    std::thread t1(function_1);

    for (int i=0; i<100; ++i)
    {
        shared_print(std::string("frpm main: "), i);
    }

    t1.join();
    
    return 0; }