#include <iostream>
#include <thread>

void function_1()
{
    std::cout << "test " << '\n';
}

int main()
{
    std::thread t1(function_1);
    // t1.join();
    t1.detach();
    // t1.join(); // once attach forever attached. crash happens
    if(t1.joinable())
    {
        t1.join();
    }
    return 0;
}