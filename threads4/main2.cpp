#include <iostream>
#include <thread>

class Fctor
{
public:
    void operator()(std::string& msg)
    {
        std::cout << "t1 says: " << msg << '\n';
        msg = "Trust is the mother of deceit.";
    }
};

int main()
{
    std::string s = "Where there is no trust, there is no love.\n";
    std::cout << std::this_thread::get_id() << '\n';
    std::thread t1((Fctor()), std::ref(s));

    std::cout << t1.get_id() << '\n';

    std::thread t2 = std::move(t1);
    t2.join();

    std::cout << "main: " << s << '\n';

    return 0;
}