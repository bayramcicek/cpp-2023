#include <iostream>
#include <thread>

int main()
{
    std::cout << std::thread::hardware_concurrency() << '\n';
    // 4 -> how many threads running concurrently
    return 0;

}