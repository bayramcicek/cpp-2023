// C++ Threading #9: packaged_task

#include <condition_variable>
#include <future>
#include <iostream>
#include <mutex>
#include <string>
#include <thread>

int factorial(int n)
{
    int res = 1;

    for (int i = n; i > 1; --i)
    {
        res *= i;
    }

    std::cout << "result is: " << res << '\n';
    return res;
}

int main()
{
    std::packaged_task<int(int)> t(factorial);

    /// ...

    t(6); // in a different context
    int x = t.get_future().get(); // returned value from the factorial function

    return 0;
}