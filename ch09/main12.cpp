#include <iostream>
#include <string>

std::string &getRef();

int main()
{
    auto ref1{ getRef() };
    auto &ref2{ getRef() };
    return 0;
}