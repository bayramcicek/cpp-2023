#include <iostream>

int main()
{
    int x{ 5 };
    int *ptr{ &x };

    std::cout << *ptr << '\n';

    int y{ 6 };
    ptr = &y;

    std::cout << *ptr << '\n'; // 6

    *ptr = 45;

    std::cout << *ptr << '\n';
    std::cout << y << '\n';

    std::cout << typeid(&y).name() << '\n';
    // gcc, this prints “pi” (pointer to int) 

    return 0;
}