#include <iostream>

int main()
{
    int x{ 2 };
    int *ptr{ &x };

    if (ptr == nullptr)
        std::cout << "ptr is null" << '\n';
    else
        std::cout << "ptr is non-null" << '\n';

    int *nullPtr{};
    std::cout << "nullPtr is: " << (nullPtr == nullptr ? "null\n" : "non-null\n");

    return 0;
}