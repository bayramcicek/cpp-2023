#include <iostream>

int main()
{
    const int x{ 5 };
    const int *ptr{ &x };

    // *ptr = 45;

    const int y{ 8 };
    ptr = &y;

    int z{ 90 };
    const int *zPtr{ &z };
    // *zPtr = 66;
    z = 99;

    return 0;
}