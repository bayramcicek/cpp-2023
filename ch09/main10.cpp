#include <iostream>

void printByValue(std::string val) { std::cout << val << '\n'; }

void printByReference(const std::string &ref) { std::cout << ref << '\n'; }

void printByAddress(const std::string *ptr) { std::cout << *ptr << '\n'; }

void nullify(int *&refptr) { refptr = nullptr; }

int main()
{
    std::string str{ "Hello, World" };

    printByValue(str);
    printByReference(str);
    printByAddress(&str);

    int x{ 5 };
    int *ptr = &x;

    std::cout << "ptr is " << (ptr ? "non-null\n" : "null\n");
    nullify(ptr);
    std::cout << "ptr is " << (ptr ? "non-null\n" : "null\n");

    return 0;
}