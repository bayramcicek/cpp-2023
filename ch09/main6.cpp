#include <iostream>

int main()
{
    int *ptr{};
    int x{ 5 };
    ptr = &x;

    std::cout << *ptr << '\n';

    int *nPtr{ nullptr };
   // std::cout << "nulptr: " << *nPtr << '\n'; // Segmentation fault (core dumped)

    int value{ 3 };
    int *ptr2{ &value };

    ptr2 = nullptr;

    return 0;
}