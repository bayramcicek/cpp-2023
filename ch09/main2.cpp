#include <iostream>
#include <string>

void printValue(std::string &x) 
{ 
    std::cout << x << '\n';
    x  = "hello";
}

int main()
{
    std::string val{ "selam" };
    printValue(val);
    std::cout << val << '\n';
    return 0;
}