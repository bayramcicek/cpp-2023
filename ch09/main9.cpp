#include <iostream>

int main()
{
    int x{ 1 };
    int *const ptr{ &x };

    int y{ 5 };
    int z{ 6 };

    int *const ptr1{ &y };
    // ptr1 = &z;
    *ptr1 = 90;

    std::cout << *ptr1 << '\n';

    const int myVal{ 42 };
    const int *const myPtr{ &myVal };

    std::cout << myPtr << ' ' << *myPtr << '\n';
 
    return 0;
}