#include <iostream>

int main()
{
    int x{ 5 };
    // std::cout << x << '\n';
    std::cout << &x << '\n';

    int y{ 4 };
    // std::cout << y << '\n';
    std::cout << &y << " : " << *(&y) << '\n';

    int &z{ y };
    int *ptr{ &x };

    std::cout << ptr << '\n';
    std::cout << *ptr << '\n';

    return 0;
}