#include <iostream>

int main()
{
    char *chPtr{};
    int *iPtr{};
    long double *lPtr{};

    std::cout << sizeof(chPtr) << '\n'; // 8
    std::cout << sizeof(iPtr) << '\n'; // 8
    std::cout << sizeof(lPtr) << '\n'; // 8
    return 0;
}