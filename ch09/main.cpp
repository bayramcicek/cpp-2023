#include <iostream>

int main()
{
    const int val1{ 54 };
    const int &refVal{ val1 };
    std::cout << refVal << '\n';

    /*
    int x{ 5 };
    int &ref{ x };

    std::cout << x << '\n';
    std::cout << ref << '\n';

    ref = 7;
    std::cout << ref << '\n';
    std::cout << x << '\n';
    */

    return 0;
}