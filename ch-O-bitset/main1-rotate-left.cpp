#include <bitset>
#include <iostream>

// "rotl" stands for "rotate left"
std::bitset<4> rotl(std::bitset<4> bits)
{
    // Your code here
    const std::bitset<4> one{ 0b0001 };
    const std::bitset<4> leftOne{ 0b1000 };
    const std::bitset<4> isTrue { bits & leftOne };

    if (isTrue.to_ulong())
    {
        return ((bits << 1) | one);
    }
    else
    {
        return bits << 1;
    }
}

int main()
{
    std::bitset<4> bits1{ 0b0001 };
    std::cout << rotl(bits1) << '\n';

    std::bitset<4> bits2{ 0b1001 };
    std::cout << rotl(bits2) << '\n';

    return 0;
}