#include <iostream>
#include <cmath>

void numberToBinary(int number)
{
    int num = number;
    for (int i = 7; i >= 0; i--)
    {
        int temp = std::pow(2, i);

        if (num >= temp)
        {
            std::cout << "1" << ' ';
            num -= temp;
        }
        else
            std::cout << "0" << ' ';
    }
}

int main()
{
    std::cout << "input number [0 - 255]: ";
    int number {};
    std::cin >> number;

    numberToBinary(number);

    return 0;
}