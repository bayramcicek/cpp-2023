#include <bitset>
#include <iostream>

int main()
{
    std::bitset<8> bits { 0b0000'0101 };
    bits.set(3);
    bits.flip(4);
    bits.reset(4);

    std::cout << "bits: " << bits << '\n';
    std::cout << "bit 3: " << bits.test(3) << '\n';
    std::cout << "bit 4: " << bits.test(4) << '\n';

    std::bitset<4> x { 0b1100 };
    std::cout << x << '\n';         // 1100
    std::cout << (x >> 1) << '\n';  // 0110
    std::cout << (x << 1) << '\n';  // 1000

    return 0;
}