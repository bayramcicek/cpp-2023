#include <cstdint>
#include <iostream>

int main()
{
    constexpr std::uint8_t mask0{ 0b0000'0001 }; // represents bit 0
    constexpr std::uint8_t mask1{ 0b0000'0010 }; // represents bit 1
    constexpr std::uint8_t mask2{ 0b0000'0100 }; // represents bit 2
    constexpr std::uint8_t mask3{ 0b0000'1000 }; // represents bit 3
    constexpr std::uint8_t mask4{ 0b0001'0000 }; // represents bit 4
    constexpr std::uint8_t mask5{ 0b0010'0000 }; // represents bit 5
    constexpr std::uint8_t mask6{ 0b0100'0000 }; // represents bit 6
    constexpr std::uint8_t mask7{ 0b1000'0000 }; // represents bit 7

    std::uint8_t flags{ 0b0001'0101 };
    std::cout << "bit 0: " << ((flags & mask0) ? "on\n" : "off\n");
    flags |= mask1; // turn on bit 1
    flags &= ~mask4; // turn off bit 4
    std::cout << "bit 1: " << ((flags & mask1) ? "on\n" : "off\n");
    std::cout << "bit 4: " << ((flags & mask4) ? "on\n" : "off\n");
    flags ^= mask4; // flip bit 4
    std::cout << "bit 4: " << ((flags & mask4) ? "on\n" : "off\n");

    return 0;
}