#include <iostream>

class Cents
{
private:
    int m_cents{};

public:
    Cents(int cents)
        : m_cents{ cents }
    {
    }

    friend Cents operator+(const Cents &c1, const Cents &c2);
    int getCents() const { return m_cents; }
};

Cents operator+(const Cents &c1, const Cents &c2)
{
    return c1.m_cents + c2.m_cents;
}

int main()
{
    Cents cents1{ 8 };
    Cents cents2{ 50 };

    std::cout << (cents1 + cents2).getCents() << '\n';

    return 0;
}