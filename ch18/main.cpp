#include <iostream>
#include <string_view>

class Base
{
public:
    virtual std::string_view getName() const { return "Base"; }
};

class Derived : public Base
{
public:
    virtual std::string_view getName() const { return "Derived"; }
};

int main() { 
    
    Derived d{};
    Base& r{ d };
    std::cout << "r is: " << r.getName() << '\n';
    
    return 0; }