#include <fstream>
#include <iostream>
#include <string>

int main()
{
    std::ifstream inf{ "test.txt" };

    if (!inf)
    {
        std::cerr << "Uh oh, test.txt could not be opened for reading!\n";
        return 1;
    }

    inf.seekg(0, std::ios::end); // move to end of file
    std::cout << inf.tellg() << '\n';

    std::string strData;

    inf.seekg(5);
    std::getline(inf, strData);
    std::cout << strData << '\n';

    inf.seekg(8, std::ios::cur);
    std::getline(inf, strData);
    std::cout << strData << '\n';

    inf.seekg(-15, std::ios::cur);
    std::getline(inf, strData);
    std::cout << strData << '\n';

    return 0;
}