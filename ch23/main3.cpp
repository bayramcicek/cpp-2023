#include <iostream>
#include <sstream>

int main()
{
    std::stringstream os;
    os << "Hello ";

    os.str(""); // erase the buffer
    os.clear(); // reset error flags

    os << "World!";
    std::cout << os.str();

    return 0;
}