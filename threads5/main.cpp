#include <iostream>
#include <thread>
#include <vector>

std::vector<std::thread> t;

void foo(int size)
{
    for (int i = 0; i < 300; ++i)
    {
        std::cout << size + i << '\n';
    }
}

void testFunction()
{
    std::cout << "start testFunction" << '\n';

    std::thread th(foo, 20);
    t.push_back(std::move(th)); //<=== move (after, th doesn't hold it anymore
    std::cout << "Thread started" << std::endl;

    std::cout << "out testFunction" << '\n';
}

int main()
{
    testFunction();
    
    for (auto &th : t)
    { //<=== range-based for uses & reference
        th.join();
    }

    return 0;
}