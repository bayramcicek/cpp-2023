#include <exception>
#include <iostream>
#include <stdexcept>

class Fraction
{
private:
    int m_numerator = 0;
    int m_denominator = 1;

public:
    Fraction(int x = 0, int y = 1)
        : m_numerator{ x }
        , m_denominator{ y }
    {
        if (m_denominator == 0)
            throw std::runtime_error("Invalid denominator");
    }

    friend std::ostream &operator<<(std::ostream &out, const Fraction &f1);
};

std::ostream &operator<<(std::ostream &out, const Fraction &f1)
{
    out << f1.m_numerator << '/' << f1.m_denominator;
    return out;
}

int main()
{
    std::cout << "Enter the numerator: ";
    int numerator{};
    std::cin >> numerator;

    std::cout << "Enter the denominator: ";
    int denominator{};
    std::cin >> denominator;

    try
    {
        Fraction f{ numerator, denominator };
        std::cout << "your fraction is: " << f << '\n';
    }
    catch (const std::exception &e)
    {
        std::cerr << e.what() << '\n';
    }

    return 0;
}